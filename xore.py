from bottle import route, run, template, error
from datetime import datetime


cfile = open("debug.txt", "r")  # Open Config File

D = cfile.read()    # Debug Mode
L = '[LOG]:'    # Log Exstensions
M = '[MSG]:'    # Log Exstensions
H = 'localhost' # Host Name



if D == 'True':
    # Port Number for Debug
    P = 1234
    print(L + 'Debug Active')
    @route('/debug')
    def debug():
        return '<h1>Debug Mode Active</h1>'
else:
    # Port Number for Production
    P = 8080
    @route('/debug')
    def debug():
        return '<h1>Debug: Inactive</h1>'

@route('/hello/<name>')
def hello(name):
    return template('<b>Hello {{name}}</b>!', name=name)

@route('/systime')
def systime():
    DTC = datetime.isoformat()
    return DTC

@error(404)
def err404(error):
    return "<h1>Not Found, Bummer...</h1>"

@route('/')
def index():
    if D == True:
        return 'Goto /debug'
    else:
        return 'Goto /hello/<name>' 
    
run(host=H, port= P)
